const brwSync = require('browser-sync');

brwSync({
    // server: "app",
    server: "./app/pages",
    files: ["./app/pages/**/*.html", "./app/pages/css/**/*.css", "./app/pages/script/**/*.js"],
}); 