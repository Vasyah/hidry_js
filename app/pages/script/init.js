import { get, getJSON } from './server.js';

class Initiator {
    cards = [];
    constructor() {
    }

    getDataItem(id){
        return this.cards.find(item => item.id === id);
    }
    getDataArr(arr){
        return this.cards.filter(item => {
            return arr.some(i => i === item.id )
        });
    }
}

export { Initiator }