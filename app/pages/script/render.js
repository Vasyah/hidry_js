function renderCard(tmplEl, mountEl, data, currency) {
    const tmplt = document.querySelector(`#${tmplEl}`);
    const item = tmplt.content.cloneNode(true);
    item.querySelector('.img').dataset.cardId = data.id;
    item.querySelector('.img').dataset.cardId = data.id;
    item.querySelector(".img").src = data.imgPath;
    item.querySelector(".img").alt = data.name;
    item.querySelector(".card-body__title").textContent = data.name;
    item.querySelector(".card-body__price--val").textContent = data.price;
    item.querySelector(".card-body__price--currency").textContent = ` ${currency}`;
    item.querySelector(".btn-card").dataset.cardId = data.id;
    item.querySelector(".btn-card").dataset.cardPrice = data.price;
    mountEl.appendChild(item);
}

function renderCardInfo(tmplEl, mountEl, data, currency) {
    const tmplt = document.querySelector(`#${tmplEl}`);
    const item = tmplt.content.cloneNode(true);
    item.querySelector('.product-card__img').dataset.cardId = data.id;
    item.querySelector(".product-card__img").src = data.imgPath;
    item.querySelector(".product-card__img").alt = data.name;
    item.querySelector(".card-body__title").textContent = data.name;
    item.querySelector(".card-body__price--val").textContent = data.price;
    item.querySelector(".card-body__price--currency").textContent = ` ${currency}`;
    mountEl.appendChild(item);
}
function placeTmpl(tmpl){
    document.body.insertAdjacentHTML("afterBegin", tmpl);
}

export { renderCard, placeTmpl}

// old
// function markupCard(data,currency) {
//     const card = `
//         <div class="grid-item grid-card cards">
//             <article class="cards__item card">
//                 <div class="img-wrapper">
//                     <div class="img-inner">
//                         <img src="${data.imgPath}" alt="${data.name}" class="img">
//                     </div>
//                 </div>
//                 <div class="card-body">
//                     <h3 class="card-body__title">${data.name}</h3>
//                     <div class="card-body__price"><span class="card-body__price--val">${data.price}</span><span
//                             class="card-body__price--currency">${currency}</span>
//                     </div>
//                     <div class="card-body__btn">
//                         <button class="btn btn--s btn-card">В корзину</button>
//                     </div>
//                 </div>
//             </article>
//         </div>
//     `;
//     return card;
// }


