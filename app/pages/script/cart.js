
export class CartMin {
    constructor() {
        this.mainEl = document.querySelector('.nav__item--cart .nav__link');
        this.goods = null;
    }

    init(data) {
        this.getData(data);
        this.setTotal();
        this.render();
    }

    getData(data) {
        return this.goods = data
    }

    setTotal() {
        // TODO: добавить преведение к виду 1 000 000
        this.totalPrice = this.goods.reduce((sum, item) => {
            return sum += item.value * item.price;
        }, 0);
        this.totalCount = this.goods.reduce((sum, item) => {
            return sum += item.value;
        }, 0);
    }

    render() {
        this.mainEl.style.position = "relative";
        this.mainEl.querySelector('span').remove();
        const isRender = this.mainEl.querySelector("div") || false;
        if (isRender) {
            isRender.remove();
        }
        const html = `
        <span>Корзина (${this.totalCount})</span>
        <div style="position:absolute;top: -25%;left:-25%;background-color:var(--main-color);padding:3px;border-radius:3px">${this.totalPrice} руб.</div>
        `
        this.mainEl.insertAdjacentHTML("beforeEnd", html);
    }
}


export class CartMax {
    mainEl = null;
    mainChildEl = null;
    goods = null;

    totalPrice = null;
    totalCount = null;

    constructor() {

    }
    init(_strg, data) {
        this.mainEl = document.querySelector(".cart");
        this.mainChildEl = document.querySelector(".cart__products");
        this.orderInfo = document.querySelector(".purchase-total");
        this.setData(_strg);
        this.setTotal();
        this.mainEl.appendChild(this.render(data));
        this.orderInfo.appendChild(this.renderOrderInfo());
        // this.getData(data);
        // this.render();
    }

    setData(data) {
        return this.goods = data
    }

    setTotal() {
        // TODO: добавить преведение к виду 1 000 000
        this.totalPrice = this.goods.reduce((sum, item) => {
            return sum += item.value * item.price;
        }, 0);
        this.totalCount = this.goods.reduce((sum, item) => {
            return sum += item.value;
        }, 0);
    }

    addLstnr(_model, data) {

        this.mainEl.addEventListener('click', handler)

        function handler(e, _model, cartObj) {
            const id = e.target.dataset.id || null;
            if (e.target.classList.contains("js-cart-add")) {
                _model.add(id);
            }
            else if (e.target.classList.contains("js-cart-rm")) {
                _model.rm(id);
            }
            else if (e.target.classList.contains("js-cart-del")) {
                _model.del(id);
            }
            else if (e.target.classList.contains("js-cart-delAll")) {
                _model.delAll(id);
            }
            cartObj.render()
        };
    }

    render(data) {
        const tmplt = document.querySelector(`#cart`);
        const item = tmplt.content.cloneNode(true);
        item.querySelector(".cart__total--qnt").textContent = this.totalCount;
        item.querySelector(".cart__total--sum").textContent = `${this.totalPrice} ₽`;


        const HTML = data.reduce((sum, item, index) => {
            if (index === 0) {
                sum += `
                <li class="products__item product product--header">
                    <div class="product__cb">
                        <label for="id-all" class="product__label cb">
                            <input class="product__field cb__input" id="id-all" type="checkbox">
                            <div class="product__img--wrp cb__content">
                                Выбрать всё
                            </div>
                        </label>
                    </div>
                    <div class="product__del">
                        <button class="btn btn--sm product__del-btn js-cart-delAll">
                            Удалить всё
                            <svg class="" width="18" height="18">
                                <use xlink:href="../../icons/main_0.svg#trash"></use>
                            </svg>
                        </button>
                    </div>
                </li>
                `
            }
            return sum += `
            <li class="products__item product" data-card-id="${item.id}">
                <div class="product__cb">
                    <label for="id-${item.id}" class="product__label cb">
                        <input class="product__field cb__input" id="id-${item.id}" type="checkbox">
                        <div class="product__img--wrp cb__content">
                            <img src="${item.imgPath}" alt="${item.name}" class="product__img">
                        </div>
                    </label>
                </div>
                <div class="product__title">${item.name}</div>
                <div class="product__qnt product-qnt">
                    <button class="product-qnt__btn btn btn--sm js-cart-rm" data-card-id="${item.id}">-</button>
                    <span class="product-qnt__item">${this.goods.find(i => i.id === item.id).value}</span>
                    <button class="product-qnt__btn btn btn--sm js-cart-add" data-card-id="${item.id}">+</button>
                </div>
                <div class="product__price">${item.price}
                    <span class="product__price--currency">₽</span>
                </div>
                <div class="product__cart-icon">
                    <button class="btn btn--sm btn--f js-cart-del">
                        <svg class="" width="18" height="18">
                            <use xlink:href="../../icons/main_0.svg#trash"></use>
                        </svg>
                    </button>
                </div>
            </li>
            `
        }, "");
        item.querySelector('.cart__products').insertAdjacentHTML('afterBegin', HTML);

        return item;
    }

    renderOrderInfo() {
        const tmplt = document.querySelector("#purchase");
        const item = tmplt.content.cloneNode(true);
        item.querySelector('.js-cart-total').textContent = `${this.totalPrice} ₽`;
        // TODO: добавить прибавление стоимости доставки
        item.querySelector('.js-cart-final').textContent =  `${this.totalPrice} ₽`;;
        return item;
    }
}