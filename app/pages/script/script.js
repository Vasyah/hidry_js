'use strict'
import { Initiator } from './init.js';
import { get, getJSON } from './server.js';
import { renderCard, placeTmpl } from './render.js';
import { Card, Cards } from './card.js';
// import { renderCard } from './card.js';
// import { goURL } from './route.js'
import { Filter } from './filter.js'
import { highlightMode } from './task_01.js'
// CART
import { CartStrg_model } from './cartStrg.js'
import { CartMin, CartMax } from './cart.js'
const cardsURL = "http://localhost:3000/api/cards.json";
const dictURL = "http://localhost:3000/api/dictionary.json";
const crdTmpl = "http://localhost:3000/tmpl/card.html";
const crdInfoTmpl = "http://localhost:3000/tmpl/card-info.html";
const prdPage = "/card-info/index.html";
const ordrPage = "/cart/index.html";


const MAIN = new Initiator();
const cardsCl = new Cards();
const cartStrg_model = new CartStrg_model();
const cartMin = new CartMin();

getJSON(cardsURL).then(data => {
    // get data
    MAIN.cards = data.cards;
    getJSON(dictURL).then(dictionary => {
        MAIN.dictionary = dictionary;
        cartMin.init(cartStrg_model.getLS());

        if (document.location.pathname === "/") {
            get(crdTmpl).then(tmpl => {
                placeTmpl(tmpl);
                cardsCl.addEl(document.querySelector('.content'));
                MAIN.cards.forEach(i => {
                    const card = new Card();
                    card.setData(i, MAIN.dictionary);
                    cardsCl.addData(card);
                });
                const filter = new Filter();
                filter.init(MAIN.cards);
                filter.filterLstnr(filter, cardsCl, MAIN.dictionary);
                cardsCl.renderCards(prdPage, "product-card");
                cardsCl.mainEl.addEventListener('click', function (e) {
                    if (e.target.classList.contains('btn-card')) {
                        cartStrg_model.add(MAIN.getDataItem(e.target.dataset.cardId));
                        cartMin.init(cartStrg_model.getLS());
                    }
                });
            });
        }
        else if (document.location.pathname === "/card-info/index.html") {
            get(crdInfoTmpl).then(tmpl => {
                placeTmpl(tmpl);
                const currEl = document.location.search.split('=')[1] - 1;
                cardsCl.addEl(document.querySelector('.content'));
                const card = new Card();
                card.setData(MAIN.cards[currEl], MAIN.dictionary);
                cardsCl.addData(card);
                cardsCl.mainEl.appendChild(card.renderCardInfo("product-info"));
                card.mainEl = cardsCl.mainEl.querySelector(`[data-card-id="${card.id}"]`);
                card.goPageLstnr(prdPage);
                cardsCl.mainEl.addEventListener('click', function (e) {
                    if (e.target.classList.contains('js-btn-add')) {
                        cartStrg_model.add(MAIN.getDataItem(e.target.dataset.cardId));
                        cartMin.init(cartStrg_model.getLS());
                    }
                    if (e.target.classList.contains('js-btn-order')) {
                        return document.location.href = ordrPage;
                    }
                });
            });
        }
        else if (document.location.pathname === "/cart/index.html") {
            get(crdTmpl).then(tmpl => {
                const cartMax = new CartMax();
                cartMax.init(cartStrg_model.getLS(), MAIN.getDataArr(cartStrg_model.getLS().map(i => i.id)));
            });
        }
    });
});

document.addEventListener('DOMContentLoaded', function () {
    if (document.querySelector('.filter')) {
        let filterBtn = document.querySelector('.btn-filter');
        let filterClose = document.querySelector('.filter__btn');
        let filter = document.querySelector('.filter');
        filterBtn.addEventListener('click', function () {
            filter.classList.add('show');
        });
        filterClose.addEventListener('click', function () {
            filter.classList.remove('show');
        });
    }
    let headerCartBtn = document.querySelector('.nav__item--cart');
    let cart = document.querySelector('.cart');
    let cartBtn = document.querySelector('.cart__btn');
    let menuBtn = document.querySelector('.btn-main--catalog');
    let menu = document.querySelector('.header__nav');
    // filterBtn.addEventListener('click', function(){
    //     filter.classList.add('show');
    // })

    headerCartBtn.addEventListener('click', function () {
        return document.location.href = ordrPage;
    });
    cartBtn.addEventListener('click', function () {
        cart.classList.remove('show');
    });
    menuBtn.addEventListener('click', function () {
        menu.classList.toggle('show');
    });
});


highlightMode();
// нужно сделать открытие корзины, переход в основную корзину,
// малая корзинка правильно хранится

// сама модель корзины должна хранить данные в LS 

// мы пишем прцоедурный код - не функциональный

