'use strict'


function getValue(arr, param) {
    return arr.map(i => i[param]);
}

class Weight {
    start = null;
    end = null;
    constructor(val) {
    }
    get(param) {
        return this[param];
    }
    set(val, param) {
        if (val === "" || isNaN(val)) return this[param] = null;
        return this[param] = +val;
    }
}
class Price {
    constructor(val) {
        this.price = +val;
    }
    get() {
        return this.price;
    }
    set(val) {
        if (val === "") return this.price = 0;
        return this.price = +val;
    }
}


class Timer {
    timerid = null;
    constructor() {
    }
    setID(e) {
        this.timerid = e;
        console.log(this.timerid);
    }
    getID() {
        return this.timerid
    }
    isStart() {
        return this.timerid === null ? false : true;
    }
}

export class Filter {
    type = [];
    character = [];
    weight = new Weight();
    priceFrom = [];
    priceTo = [];
    filterList = [];
    mainEl = null;
    timerId = null;
    filterTimer = new Timer();
    data = []

    constructor() {
    }
    init(data) {
        this.data = data;
        this.mainEl = document.querySelector('.filter');
        const typeEl = this.mainEl.querySelector("[data-filter='type']").querySelectorAll('input');
        const characterEl = this.mainEl.querySelector("[data-filter='character']").querySelectorAll('input');
        const weightEl = this.mainEl.querySelector("[data-filter='weight']").querySelectorAll('input');
        typeEl.forEach(el => el.classList.add('js-type-item'));
        characterEl.forEach(el => el.classList.add('js-character-item'));
        weightEl.forEach(el => el.classList.add('js-weight-item'));

    }
    filterLstnr(filterObj, cardsObj) {
        this.mainEl.addEventListener('change', filter);
        function filter(e) {

            if (e.target.classList.contains('js-type-item')) {
                if (filterObj.filterTimer.isStart) {
                    clearTimeout(filterObj.filterTimer.getID());
                }
                let chBoxChecked = Array.from(e.currentTarget.querySelectorAll('.cb__input:checked'));
                filterObj.type = getValue(chBoxChecked, "id");
                console.log(`fitler type: ${filterObj.type}`);
                if (!e.target.checked) return;
                filterObj.timerId = setTimeout(() => {
                    filterObj.renderBtn(filterObj, e.target.nextElementSibling, cardsObj);
                }, 2000);
                console.dir(this);
                console.log(filterObj);
            }
            else if (e.target.classList.contains('js-character-item')) {
                if (filterObj.filterTimer.isStart) {
                    clearTimeout(filterObj.filterTimer.getID());
                }
                let chBoxChecked = Array.from(e.currentTarget.querySelectorAll('.cb__input:checked'));
                filterObj.character = getValue(chBoxChecked, "id");
                if (!e.target.checked) return;
                console.log(`character: ${filterObj.character}`);
                filterObj.filterTimer.setID(setTimeout(() => {
                    filterObj.renderBtn(filterObj, e.target.nextElementSibling, cardsObj);
                }, 2000));
            }
            else if (e.target.classList.contains('js-weight-item')) {
                if (filterObj.filterTimer.isStart) {
                    clearTimeout(filterObj.filterTimer.getID());
                }
                if (e.target.id === "filter-start") {
                    filterObj.weight.set(e.target.value, "start");
                    console.log("start:", filterObj.weight.start);
                }
                else {
                    filterObj.weight.set(e.target.value, "end");
                    console.log("end:", filterObj.weight.end);
                }
                filterObj.filterTimer.setID(setTimeout(() => {
                    filterObj.renderBtn(filterObj, e.target.parentElement.parentElement, cardsObj);
                }, 2000));
            }
        }
    }
    setFilterData(dataAll) {
        this.filterList = dataAll.filter(data => {
            if (!this.type.length) return true;
            return this.type.some(type => {
                return data.type.params.some(param => {
                    return param.value === type;
                });
            })
        })
            .filter(data => {
                if (!this.character.length) return true;
                return this.character.some(type => {
                    return data.character.params.some(param => {
                        return param.value === type;
                    });
                })
            })
            .filter(data => {
                const cardWeightStart = Number(data.weight.params[0].value.split('(')[1].slice(0, -1).split('-')[0]);
                const cardWeightEnd = Number(data.weight.params[0].value.split('(')[1].slice(0, -1).split('-')[1].slice(0, -3));
                const filterWeightStart = this.weight.start;
                const filterWeightEnd = this.weight.end;
                // если оба значения не заданы
                if (filterWeightStart === null && filterWeightEnd === null) {
                    return true;
                }
                // если задано только значени "От"
                else if (filterWeightEnd === null) {
                    if (filterWeightStart <= cardWeightStart || filterWeightStart <= cardWeightEnd) {
                        return true
                    }
                }
                // если задано только значени "До"
                else if (filterWeightStart === null) {
                    if (filterWeightEnd >= cardWeightStart || filterWeightEnd >= cardWeightEnd) {
                        return true
                    }
                }
                else {
                    if (filterWeightEnd < filterWeightStart) {
                        throw new Error("Ошибка");
                    }
                    if (filterWeightStart < cardWeightStart) {
                        if (filterWeightEnd < cardWeightStart) {
                            // вне диапазона
                            return false
                        }
                        if (filterWeightEnd >= cardWeightStart || filterWeightEnd <= cardWeightEnd || filterWeightEnd > cardWeightEnd) {
                            return true;
                        }
                    }
                    else if (filterWeightStart >= cardWeightStart) {
                        if (filterWeightStart > cardWeightEnd) {
                            return false;
                        }
                        if (filterWeightEnd >= cardWeightStart || filterWeightEnd <= cardWeightEnd || filterWeightEnd > cardWeightEnd) {
                            return true
                        }
                    }
                }

            });
    }
    filterBtnLstnr(filterObj, cardsObj) {
        const filterBtn = document.querySelector('.btn-main.block');
        filterBtn.addEventListener('click', function (e) {
            cardsObj.cards = filterObj.filterList;
            document.querySelector('.content').innerHTML = "";
            cardsObj.renderCards("/card-info/index.html", "product-card");
            e.currentTarget.remove();
        });

    }
    renderBtn(filterObj, e, cardsObj) {
        const oldBtn = document.querySelector('.btn-main.block') || false;
        if (oldBtn) {
            oldBtn.remove();
        }
        const isEmpty = !filterObj.type.length && !filterObj.character.length && filterObj.weight.start === null && filterObj.weight.end === null;
        if (isEmpty) return;
        filterObj.setFilterData(cardsObj.cards);
        const tmplt = document.querySelector('#filter-tip');
        const fragment = document.createDocumentFragment();
        const item = tmplt.content.cloneNode(true);
        item.querySelector('.block').style.right = "0px";
        item.querySelector('.block').style.top = "50%";
        item.querySelector('.block__num').textContent = `${filterObj.filterList.length}`;
        e.style.position = 'relative';
        e.appendChild(item);
        filterObj.filterBtnLstnr(filterObj, cardsObj);
    }

    render(tmplEl) {
        const tmplt = document.querySelector(`#${tmplEl}`);
        const item = tmplt.content.cloneNode(true);

        item.querySelector(".product-card__img").src = this.imgPath;
        item.querySelector(".product-card__img").alt = this.title;
        item.querySelector(".product-info__title").textContent = `Характеристика ${this.title}`
        const cardInfoList = [this.type, this.weight, this.character, this.ill, this.int];
        console.log(cardInfoList);

        const HTML = cardInfoList.reduce((sum, item) => {
            const paramsStr = item.params.reduce((sum, param, index) => {
                if (index > 0) {
                    return sum.concat(`${param.value.toLowerCase()}, `);
                }
                return sum.concat(`${param.value}, `);

            }, '').replace(/,\s*$/, ""); // здесь собираем параметры в строку
            return sum += `
                <li class="product-info__item">
                    <span class="product-info__item--sm">${item.dictName}</span>
                    <span class="product-info__item--md">${paramsStr}</span>
                </li>
            `
        }, '');
        console.log(HTML);
        item.querySelector('.product-info').insertAdjacentHTML('afterBegin', HTML);
        item.querySelector(".product-info__price--val").textContent = this.price;
        item.querySelector(".product-info__price--currency").textContent = this.currency;
        item.querySelector(".js-btn-add").dataset.cardId = this.id
        item.querySelector(".js-btn-add").dataset.cardPrice = this.price;
        return item;
    }
}
