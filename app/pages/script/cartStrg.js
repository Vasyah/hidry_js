// корзина
// добавить количество товара
// стоимость
class CartStrg_model {
    constructor() {
        this.goods = {
            cart: []
        };
        this.totalPrice = 0;
        this.totalCount = 0;
        this.strg = 0;
    }
    // нужно обновить текущий товар
    add(obj) {
        this.getLS();
        // this.goods.some(item => item.key === `id${id}`)
        const isExist = this.goods.cart.some(item => {
            return item.id === obj.id;
        });
        if (isExist) {
            this.goods.cart.find(item => item.id === obj.id).value += 1;
        }
        else {
            this.goods.cart.push({
                id: obj.id,
                value: 1,
                price: Number(obj.price)
            });
        }
        console.log("cartStrg: ", this.goods.cart);
        this.setLS(this.goods.cart, "cart");
    }
    // rmw(id){}
    // del(id){}

    setLS(e, key) {
        localStorage.removeItem(key);
        localStorage.setItem(key, JSON.stringify(e));
        console.log(`Set to LS: key: ${key} data: ${e} `);
    }
    getLS() {
        let keys = Object.keys(localStorage);
        for (let key of keys) {
            if (key === "cart") {
                this.goods.cart = (JSON.parse(localStorage.getItem(key)));
                console.log("Local storage now:", this.goods.cart);
            }
        }
        return this.goods.cart;
    }
    renderToHeader() {
        
    }
    // TODO: дбоавить minus
    // TODO: дбоавить del
    // собрать все товары и их количества
    // вывести количество
    // вывести стоимость
    // здесь в принципе вытаскивать наш объект с товаром
}

// всё пишется классами
// React переходит на хуки - это  обработчики

export { CartStrg_model }