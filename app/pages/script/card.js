

class Cards {
    constructor() {
        this.cards = [];
        this.cardsEl = null;
        this.mainEl = null;
    }

    addData(e) {
        return this.cards.push(e);
    }
    addEl(e) {
        return this.mainEl = e;
    }
    renderCards(url, tmpl){
        this.cards.sort(i => Math.random() - 0.5).forEach(card => {
            this.mainEl.appendChild(card.renderCard(tmpl));
            card.mainEl = this.mainEl.querySelector(`[data-card-id="${card.id}"]`);
            card.goPageLstnr(url);
        });

    }
}
class Card {
    id = null;
    imgPath = null;
    price = null;
    title = null;
    currency = null;

    type = null;
    weight = null;
    ill = null;
    character = null;
    int = null;
    mainEl = null;

    constructor() {
    }

    setData(data, dictionary) {
        this.id = data.id;
        this.imgPath = data.imgPath;
        this.price = data.price;
        this.title = data.name;
        this.currency = " ₽";

        this.type = this.getData(data.props.type, dictionary.type);
        this.weight = this.getData(data.props.weight, dictionary.weight);
        this.ill = this.getData(data.props.sickness, dictionary.sickness);
        this.character = this.getData(data.props.character, dictionary.character);
        this.int = this.getData(data.props.int, dictionary.int);
    }
    btnLstnr() {

    }
    getFrom(url) {

    }
    goPageLstnr(url) {
        try {
            const id = this.id;
            this.mainEl.addEventListener('click', goUrl);
            function goUrl(e) {
                // if (e.target.classList.contains('btn-card')) {
                if (e.target.tagName === 'BUTTON') {
                    console.dir(e.target.tagName);
                    // TODO: прекращаются все события и товар не добавляется в корзину
                    // e.preventDefault();
                    // e.stopImmediatePropagation();
                    return;
                }
                return document.location.href = `${url}?id=${id}`;
            }
        } catch (e) {
            console.log("ERROR TO ADD LISTENER");
        }
    }
    getData(params, dictionary) {
        return params.reduce((sum, param) => {

            dictionary.forEach(dictEl => {

                if (dictEl.dictName) {
                    sum.dictName = dictEl.dictName;
                }
                if (param === dictEl.id) {
                    sum.params.push(dictEl);
                }
            });
            return sum;
        },
            {
                params: []
            });
    }
    renderCard(tmplEl) {
        const tmplt = document.querySelector(`#${tmplEl}`);
        const item = tmplt.content.cloneNode(true);
        item.querySelector(".cards").dataset.cardId = this.id
        item.querySelector(".img").src = this.imgPath;
        item.querySelector(".img").alt = this.title;
        item.querySelector(".card-body__title").textContent = this.title;
        item.querySelector(".card-body__price--val").textContent = this.price;
        item.querySelector(".card-body__price--currency").textContent = this.currency;
        item.querySelector(".btn-card").dataset.cardId = this.id
        item.querySelector(".btn-card").dataset.cardPrice = this.price;
        return item;
    }

    renderCardInfo(tmplEl) {
        const tmplt = document.querySelector(`#${tmplEl}`);
        const item = tmplt.content.cloneNode(true);

        item.querySelector(".product-card__img").src = this.imgPath;
        item.querySelector(".product-card__img").alt = this.title;
        item.querySelector(".product-info__title").textContent = `Характеристика ${this.title}`
        // item.querySelector(".product-info").textContent = `Характеристика ${this.title}`
        const cardInfoList = [this.type, this.weight, this.character, this.ill, this.int];
        console.log(cardInfoList);

        const HTML = cardInfoList.reduce((sum, item) => {
            const paramsStr = item.params.reduce((sum, param, index) => {
                if (index > 0) {
                    return sum.concat(`${param.value.toLowerCase()}, `);
                }
                return sum.concat(`${param.value}, `);

            }, '').replace(/,\s*$/, ""); // здесь собираем параметры в строку
            return sum += `
                <li class="product-info__item">
                    <span class="product-info__item--sm">${item.dictName}</span>
                    <span class="product-info__item--md">${paramsStr}</span>
                </li>
            `
        }, '');
        console.log(HTML);
        item.querySelector('.product-info').insertAdjacentHTML('afterBegin', HTML);
        item.querySelector(".product-info__price--val").textContent = this.price;
        item.querySelector(".product-info__price--currency").textContent = this.currency;
        item.querySelector(".js-btn-add").dataset.cardId = this.id
        item.querySelector(".js-btn-add").dataset.cardPrice = this.price;
        return item;
    }
}
export { Cards, Card } 