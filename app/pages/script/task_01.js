function toggleHighlight(e, state) {
    if (state === 'on') {
        if (e.classList.contains("highlighted")) return;
        // сохраняем текущее значение цвета
        toggleAttr(e, "bgc", getComputedStyle(e).backgroundColor, "on");
        e.style.backgroundColor = `${getRndCol()}`;
        e.classList.add("highlighted");
    }
    else {
        if (e.classList.contains("highlighted")) {
            // забираем сохр. значение
            console.log(`currBGC = ${e.style.backgroundColor}`);
            e.style.backgroundColor = e.dataset.bgc;
            toggleAttr(e, "bgc");
            console.log(`restoredBGC = ${e.style.backgroundColor}`);
            e.classList.remove("highlighted");
        }
        else {
            // console.log("FAIL");
        }

    }
}

// запоминаем цвет || удаляем
function toggleAttr(e, attr, val = 0, state = "off") {
    if (state === 'on') {
        e.dataset[attr] = `${val}`;
        return;
    }
    e.removeAttribute(`data-${attr}`);
}

function getRndVal(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function getRndCol() {
    const r = Math.floor(Math.random() * (256)),
        g = Math.floor(Math.random() * (256)),
        b = Math.floor(Math.random() * (256));
    return '#' + r.toString(16) + g.toString(16) + b.toString(16);
}

export function highlightMode() {
    document.addEventListener("DOMContentLoaded", function (e) {
        document.addEventListener('keypress', function (e) {
            if (e.key === "f" || e.key === "а") {
                const allDOM = document.querySelectorAll('*');
                const allDOMLength = allDOM.length;
                // part#1
                toggleHighlight(allDOM[getRndVal(allDOMLength)], "on");

                setInterval(() => {
                    toggleHighlight(allDOM[getRndVal(allDOMLength)], "on");
                }, 2000);
                setInterval(() => {
                    toggleHighlight(allDOM[getRndVal(allDOMLength)], "off");
                }, 2000);
            }
        });
    });
}
