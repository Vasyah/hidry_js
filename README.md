# Работа с проектом

 1. сделать git clone https://Vasyah@bitbucket.org/Vasyah/hidry_js.git
 2. npm i - устанавливаем необходимые пакеты
 3. npm start - запускаем проект
### Структура файлов

app/pages - основная папка проекта

/ - разметка главной страницы
/card - разметка страницы отдельного товара
/cart - разметка страницы корзины

#### Включение highlight mode (режим подсветки)
Нажать клавишу f 
#### Отключение highlight mode только через перезагрузку страницы

### Требования
1. Подсветка элемента - реаизовано (реализация без разрушений - реализовано, включение - реализовано/отключение - через перезагрузку страницы)
2. Каталог товаров - реализовано
3. Фильтр - реализовано (данные фильтра хранятся в урле - не реализовано)
4. Карточка товара - реализовано
5. Все данные в JSON файлах - реализовано
6. Корзина малая - реализовано (подсчёт стоимости/количества - реализовано)
7. Корзина большая - рендер реализован только при обновлении страницы (подсчёт стоимости, количества - реализовано, выбор товаров/изменение количества/удаление - не реализовано)
8. Работа со справочниками - реализовано
9. Модули - реализовано (каждый объединяет только сущности из одной обалсти - частично реализовано)
10. Классы - реализовано (фичи поделены на компоненты - частично реализовано)
11. LocalStorage - реализовано (обработка изменений из соседних вкладок - не реализовано)
12. Промисы - реализовано